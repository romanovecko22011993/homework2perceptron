import numpy as np
from sklearn.datasets import make_regression
X, y = make_regression(n_samples=100, n_features=100)
X.shape, y.shape
Y=y.reshape((100, 1))
learning_rate=l1=0.1000
b1=np.random.random((100,128))
b2=np.random.random((128,50))
b3=np.random.random((50,1))
for i in range(100):
    y1=np.dot(X, b1)
    z1=np.tanh(y1)
    y2=np.dot(z1, b2)
    z2=np.tanh(y2)
    y3=np.dot(z2, b3)
    z3=1/(1+np.exp(-y3))
    mse_loss=np.sum((y3-y)**2)
    z3_error=Y-z3
    db3=z3_error*((1/np.cosh(z3))**2)
    z2_error=np.dot(db3, b3.T)
    db2=z2_error*((1/np.cosh(z2))**2)
    z1_error=np.dot(db2,b2.T)
    f=1/(1+np.exp(-z1))
    db1=z1_error*(f*(1-f))
    b3+=np.dot(z2.T,db3)
    b2+=np.dot(z1.T, db2)
    b1+=np.dot(X.T, db1)
print(z3)